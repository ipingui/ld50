using System.Linq;
using Enums;
using Game;
using IO;
using Sound;
using UnityEngine;
using VisualNovel.Background;
using VisualNovel.Dialogue.Models;
using VisualNovel.Transitions;
using CharacterController = VisualNovel.Character.CharacterController;

namespace VisualNovel.Dialogue
{
    public class DialogueController : MonoBehaviour
    {
        private Models.Dialogue currentDialogue;
        private int nextSentenceIndex;
        private int currentTrack;
        private bool dialogueOver;

        [SerializeField] private DialogueBox dialogueBox;
        [SerializeField] private QuestionController questionController;
        [SerializeField] private CharacterController characterController;
        [SerializeField] private BackgroundController backgroundController;
        [SerializeField] private TransitionController transitionController;

        private bool CanAdvance =>
            dialogueBox.CanAdvance && questionController.CanAdvance && !characterController.Animating && !dialogueOver;

        private void Awake()
        {
            LoadDialogue(ProgressionData.CurrentDialogueNumber);
        }

        private void Start()
        {
            backgroundController.SetBackground(currentDialogue.BackgroundId);
            DisplayNextSentence();
        }

        private void Update()
        {
            HandleDialogueAdvanceInput();
        }

        public void SetTrackAndAdvance(int track)
        {
            currentTrack = track;
            DisplayNextSentence();
        }

        private void HandleDialogueAdvanceInput()
        {
            if (!CanAdvance)
            {
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                DisplayNextSentence();
            }
        }

        private void DisplayNextSentence()
        {
            if (nextSentenceIndex >= currentDialogue.Sentences.Count)
            {
                dialogueOver = true;
            }
            else
            {
                var sentence = currentDialogue.Sentences[nextSentenceIndex];
                if (sentence.Track != currentTrack)
                {
                    SkipToNextValidSentence();
                }
            }

            if (dialogueOver)
            {
                LoadMinigameScene();
            }
            else
            {
                var sentence = currentDialogue.Sentences[nextSentenceIndex];
                DisplaySentence(sentence);
                nextSentenceIndex += 1;
                if (sentence.IsEndOfTrack)
                {
                    currentTrack = 0;
                }
            }
        }

        private void DisplaySentence(Sentence sentence)
        {
            characterController.SetCharacterSprite(sentence.CharacterSpriteId);
            characterController.DoAnimationAndEmote(sentence.CharacterAnimationId, sentence.CharacterEmoteId);

            if (sentence.QuestionId != QuestionId.None)
            {
                questionController.DisplayQuestion(sentence.QuestionId);
                var question = questionController.LoadQuestion(sentence.QuestionId);
                dialogueBox.DisplayQuestion(question);
            }
            else
            {
                dialogueBox.DisplaySentence(sentence);
            }

            if (sentence.MusicId != MusicId.None)
            {
                MusicPlayer.Instance.Play(sentence.MusicId);
            }

            if (sentence.SfxId != SfxId.None)
            {
                SfxManager.Instance.PlaySound(sentence.SfxId);
            }
        }

        private void SkipToNextValidSentence()
        {
            var nextValidSentence = currentDialogue.Sentences
                .Where(s => currentDialogue.Sentences.IndexOf(s) > nextSentenceIndex)
                .FirstOrDefault(s => s.Track == currentTrack);
            
            if (nextValidSentence == null)
            {
                dialogueOver = true;
                Debug.Log($"No sentences with track {currentTrack} remaining.");
                return;
            }

            nextSentenceIndex = currentDialogue.Sentences.IndexOf(nextValidSentence);
        }

        private void LoadMinigameScene()
        {
            if (currentDialogue.Number == -1)
            {
                StartCoroutine(transitionController.TransitionToScene("EndingSceneFail"));
                return;
            }

            if (currentDialogue.Number == 5)
            {
                StartCoroutine(transitionController.TransitionToScene("VictoryScene"));
                return;
            }
            
            StartCoroutine(transitionController.TransitionToScene("MinigameScene"));
        }

        private void LoadDialogue(int dialogueNumber)
        {
            var dialogue = FileReader.LoadScriptableObject<Models.Dialogue>($"Dialogues/Dialogue{dialogueNumber}");
            currentDialogue = dialogue;
        }
    }
}