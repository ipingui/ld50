using System.Collections;
using Enums;
using Game;
using Sound;
using TMPro;
using UnityEngine;
using VisualNovel.Dialogue.Models;

namespace VisualNovel.Dialogue
{
    public class DialogueBox : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI dialogueText;
        [SerializeField] private TextMeshProUGUI nameText;

        public bool CanAdvance => !typingText;

        private bool typingText;
        
        public void DisplaySentence(Sentence sentence)
        {
            dialogueText.text = sentence.Text;
            nameText.text = NameData.GetName(sentence.Character);

            var girlIsSpeaking = sentence.Character == CharacterId.Girl;
            StartCoroutine(TypeText(girlIsSpeaking));
        }

        public void DisplayQuestion(Question question)
        {
            dialogueText.text = question.QuestionText;
            StartCoroutine(TypeText(false));
        }

        private IEnumerator TypeText(bool girlIsSpeaking)
        {
            const float typeDelay = 0.02f;

            typingText = true;
            dialogueText.maxVisibleCharacters = 0;
            while (dialogueText.maxVisibleCharacters < dialogueText.text.Length)
            {
                dialogueText.maxVisibleCharacters += 1;
                if (dialogueText.maxVisibleCharacters % 4 == 0)
                {
                    var bloopSound = girlIsSpeaking ? SfxId.GirlTextBloop : SfxId.TextBloop;
                    SfxManager.Instance.PlaySound(bloopSound);
                }
                
                yield return new WaitForSeconds(typeDelay);
            }

            typingText = false;
        }
    }
}