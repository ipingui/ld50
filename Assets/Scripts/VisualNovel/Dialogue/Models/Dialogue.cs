using System.Collections.Generic;
using Enums;
using UnityEngine;

namespace VisualNovel.Dialogue.Models
{
    [CreateAssetMenu]
    public class Dialogue : ScriptableObject
    {
        [SerializeField] private BackgroundId backgroundId;
        [SerializeField] private int number;
        [SerializeField] private List<Sentence> sentences;

        public BackgroundId BackgroundId => backgroundId;
        public int Number => number;
        public List<Sentence> Sentences => sentences;
    }
}