using System.Collections.Generic;
using UnityEngine;

namespace VisualNovel.Dialogue.Models
{
    [CreateAssetMenu]
    public class Question : ScriptableObject
    {
        [SerializeField] private string questionText;
        [SerializeField] private List<Answer> answers;
        
        public string QuestionText => questionText;
        public List<Answer> Answers => answers;
    }
}