using System;
using UnityEngine;

namespace VisualNovel.Dialogue.Models
{
    [Serializable]
    public class Answer
    {
        [SerializeField] private int track;
        [SerializeField] private string answerText;

        public int Track => track;
        public string AnswerText => answerText;
    }
}