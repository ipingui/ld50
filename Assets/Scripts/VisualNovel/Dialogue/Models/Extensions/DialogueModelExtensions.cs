using System.Collections.Generic;
using System.Linq;

namespace VisualNovel.Dialogue.Models.Extensions
{
    public static class DialogueModelExtensions
    {
        public static string GetAnswerText(this IEnumerable<Answer> answers, int track)
        {
            return answers.First(a => a.Track == track).AnswerText;
        }
    }
}