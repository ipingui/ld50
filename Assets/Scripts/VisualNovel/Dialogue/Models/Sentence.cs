using System;
using Enums;
using UnityEngine;

namespace VisualNovel.Dialogue.Models
{
    [Serializable]
    public class Sentence
    {
        [SerializeField] private int track;
        [SerializeField] private CharacterId character;
        [TextArea(1,5)] [SerializeField] private string text;
        [SerializeField] private CharacterSpriteId characterSpriteIdId;
        [SerializeField] private SfxId sfxIdId;
        [SerializeField] private CharacterAnimationId characterAnimationId;
        [SerializeField] private CharacterEmoteId characterEmoteId;
        [SerializeField] private QuestionId questionId;
        [SerializeField] private MusicId musicId;
        [SerializeField] private bool isEndOfTrack;

        public int Track => track;
        public CharacterId Character => character;
        public string Text => text;
        public CharacterSpriteId CharacterSpriteId => characterSpriteIdId;
        public SfxId SfxId => sfxIdId;
        public CharacterAnimationId CharacterAnimationId => characterAnimationId;
        public CharacterEmoteId CharacterEmoteId => characterEmoteId;
        public QuestionId QuestionId => questionId;
        public MusicId MusicId => musicId;
        public bool IsEndOfTrack => isEndOfTrack;
    }
}