using System.Collections.Generic;
using System.Linq;
using Enums;
using IO;
using UnityEngine;
using VisualNovel.Dialogue.Models;
using VisualNovel.Ui;

namespace VisualNovel.Dialogue
{
    public class QuestionController : MonoBehaviour
    {
        [SerializeField] private Transform answerBoxContainer;
        [SerializeField] private DialogueController dialogueController;
        
        public bool CanAdvance => !displayingQuestion;

        private bool AnswerIsSelected => instantiatedAnswerBoxes.Exists(a => a.Selected);

        private List<AnswerBox> instantiatedAnswerBoxes = new List<AnswerBox>();
        private bool displayingQuestion;

        private void Update()
        {
            HandleAnswerBoxInput();
        }

        public void DisplayQuestion(QuestionId questionId)
        {
            displayingQuestion = true;
            
            var question = LoadQuestion(questionId);
            foreach (var questionAnswer in question.Answers)
            {
                var answerBoxPrefab = FileReader.LoadPrefab<AnswerBox>("AnswerBox");
                var answerBox = Instantiate(answerBoxPrefab, answerBoxContainer);
                answerBox.SetAnswer(questionAnswer);
                instantiatedAnswerBoxes.Add(answerBox);
            }
        }
        
        public Question LoadQuestion(QuestionId questionId)
        {
            return FileReader.LoadScriptableObject<Question>($"Questions/{questionId}");
        }

        private void HandleAnswerBoxInput()
        {
            if (!AnswerIsSelected)
            {
                return;
            }
            
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                MakeChoice();
            }
        }
        
        private void MakeChoice()
        {
            displayingQuestion = false;
            var answer = GetSelectedAnswer();
            dialogueController.SetTrackAndAdvance(answer.Track);
            RemoveAnswerBoxes();
        }

        private Answer GetSelectedAnswer()
        {
            var selectedAnswer = instantiatedAnswerBoxes.FirstOrDefault(a => a.Selected);
            return selectedAnswer != null ? selectedAnswer.Answer : new Answer();
        }

        private void RemoveAnswerBoxes()
        {
            foreach (var instantiatedAnswerBox in instantiatedAnswerBoxes)
            {
                StartCoroutine(instantiatedAnswerBox.Remove());
            }
            
            instantiatedAnswerBoxes = new List<AnswerBox>();
        }
    }
}