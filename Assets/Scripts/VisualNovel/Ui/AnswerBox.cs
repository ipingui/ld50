using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VisualNovel.Dialogue.Models;

namespace VisualNovel.Ui
{
    public class AnswerBox : SelectOnHover
    {
        [SerializeField] private TextMeshProUGUI answerText;
        public Answer Answer { get; private set; }

        private void Start()
        {
            StartCoroutine(Fade(0, 1));
        }

        public void SetAnswer(Answer answer)
        {
            Answer = answer;
            answerText.text = answer.AnswerText;
        }

        public IEnumerator Remove()
        {
            yield return Fade(1, 0);
            Destroy(gameObject);
        }
        
        private IEnumerator Fade(float startAlpha, float endAlpha)
        {
            const float fadeDuration = 0.2f;
            var timeElapsed = 0f;
            var backgroundImage = GetComponent<Image>();
            while (timeElapsed < fadeDuration)
            {
                timeElapsed += Time.deltaTime;
                
                var c = backgroundImage.color;
                var alpha = Mathf.SmoothStep(startAlpha, endAlpha, timeElapsed / fadeDuration);
                backgroundImage.color = new Color(c.r, c.g, c.b, alpha);
                answerText.alpha = alpha;
                yield return null;
            }
        }
    }
}