using System;
using Enums;
using Sound;

namespace VisualNovel.Ui
{

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public abstract class SelectOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public bool Selected { get; private set; }
        private Image image;
        private Color originalColor;
        public bool Disabled { get; set; }

        protected virtual void Awake()
        {
            image = GetComponent<Image>();
            var c = image.color;
            originalColor = new Color(c.r, c.g, c.b, 1);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Disabled)
            {
                return;
            }
        
            SfxManager.Instance.PlaySound(SfxId.Hover);
            Select();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (Disabled)
            {
                return;
            }
        
            Deselect();
        }

        public void Select()
        {
            Selected = true;
            image.color = Color.grey;
        }

        public void Deselect()
        {
            Selected = false;
            image.color = originalColor;
        }
    }
}