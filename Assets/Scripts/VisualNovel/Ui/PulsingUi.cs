using System;
using System.Collections;
using Extensions;
using UnityEngine;

namespace VisualNovel.Ui
{
    public class PulsingUi : MonoBehaviour
    {
        private void Awake()
        {
            StartCoroutine(PulseLoop());
        }

        private IEnumerator PulseLoop()
        {
            while (true)
            {
                yield return transform.ScaleCoroutine(1f, 1.05f, 1f);
                yield return transform.ScaleCoroutine(1.05f, 1f, 1f);
            }
        }
    }
}