using System;
using PinguTools;
using UnityEngine;

namespace VisualNovel.Ui
{
    public class Cursor : UiCursor
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(transform.parent.gameObject);
        }

        protected override void OnClick()
        {
        }

        protected override bool IsClickConditionSatisfied()
        {
            return false;
        }
    }
}