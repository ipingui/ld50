using System.Collections;
using Enums;
using Extensions;
using IO;
using UnityEngine;
using UnityEngine.UI;

namespace VisualNovel.Character
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField] private Image characterImage;
        [SerializeField] private CharacterAnimationController characterAnimationController;
        [SerializeField] private CharacterEmoteController characterEmoteController;

        public bool Animating => characterAnimationController.Animating || characterEmoteController.Animating || spriteFading;

        private bool spriteFading;
        private CharacterSpriteId previousCharacterSprite;
        
        public void SetCharacterSprite(CharacterSpriteId characterSprite)
        {
            if (characterSprite == CharacterSpriteId.None || characterSprite == previousCharacterSprite)
            {
                return;
            }

            previousCharacterSprite = characterSprite;
            
            var sprite = FileReader.LoadSprite(characterSprite);
            StartCoroutine(ChangeSpriteCoroutine(sprite, 0.4f));
        }

        private IEnumerator ChangeSpriteCoroutine(Sprite sprite, float duration)
        {
            spriteFading = true;
            yield return characterImage.FadeCoroutine(1, 0, duration / 2f);
            characterImage.sprite = sprite;
            yield return characterImage.FadeCoroutine(0, 1, duration / 2f);
            spriteFading = false;
        }

        public void DoAnimationAndEmote(CharacterAnimationId animationId, CharacterEmoteId emoteId)
        {
            if (animationId != CharacterAnimationId.None)
            {
                StartCoroutine(characterAnimationController.DoAnimation(animationId));
            }

            if (emoteId != CharacterEmoteId.None)
            {
                StartCoroutine(characterEmoteController.DoEmote(emoteId));
            }
        }
    }
}