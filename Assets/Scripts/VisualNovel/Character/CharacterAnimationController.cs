using System.Collections;
using Enums;
using UnityEngine;
using UnityEngine.UI;

namespace VisualNovel.Character
{
    [RequireComponent(typeof(Image))]
    public class CharacterAnimationController : MonoBehaviour
    {
        private Image characterImage;
        private Transform CharacterTransform => characterImage.transform;
        
        public bool Animating { get; private set; }

        private void Awake()
        {
            characterImage = GetComponent<Image>();
        }

        public IEnumerator DoAnimation(CharacterAnimationId animationId)
        {
            Animating = true;
            switch (animationId)
            {
                case CharacterAnimationId.Bounce:
                    yield return CharacterAnimationCoroutines.Bounce(CharacterTransform, 0.75f);
                    break;
                case CharacterAnimationId.Pulse:
                    yield return CharacterAnimationCoroutines.Pulse(CharacterTransform, 0.3f);
                    break;
            }
            Animating = false;
        }
    }
}