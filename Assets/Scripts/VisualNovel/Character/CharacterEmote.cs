using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace VisualNovel.Character
{
    [RequireComponent(typeof(Image))]
    public class CharacterEmote : MonoBehaviour
    {
        [SerializeField] private Vector3 targetOffset;
        [SerializeField] private float animationDuration;
        
        private Image emoteImage;

        private void Awake()
        {
            emoteImage = GetComponent<Image>();
        }

        public IEnumerator Animate()
        {
            yield return Fade(0, 1);

            yield return Move();

            yield return Fade(1, 0);
            
            Destroy(gameObject);
        }

        private IEnumerator Move()
        {
            var startPosition = transform.localPosition;
            var targetPosition = startPosition + targetOffset;
            var timeElapsed = 0f;
            while (timeElapsed < animationDuration)
            {
                timeElapsed += Time.deltaTime;
                transform.localPosition = Vector3.Lerp(startPosition, targetPosition, timeElapsed / animationDuration);

                yield return null;
            }
        }

        private IEnumerator Fade(float startAlpha, float endAlpha)
        {
            const float fadeDuration = 0.2f;
            var timeElapsed = 0f;
            while (timeElapsed < fadeDuration)
            {
                timeElapsed += Time.deltaTime;
                
                var c = emoteImage.color;
                var alpha = Mathf.SmoothStep(startAlpha, endAlpha, timeElapsed / fadeDuration);
                emoteImage.color = new Color(c.r, c.g, c.b, alpha);
                yield return null;
            }
        }
    }
}