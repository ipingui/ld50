using System.Collections;
using Enums;
using IO;
using UnityEngine;

namespace VisualNovel.Character
{
    public class CharacterEmoteController : MonoBehaviour
    {
        public bool Animating { get; private set; }

        private readonly Vector3 defaultEmoteLocalOffset = new Vector3(100f, 140f, 0);
        
        public IEnumerator DoEmote(CharacterEmoteId emoteId)
        {
            Animating = true;

            var emotePrefab = FileReader.LoadPrefab<CharacterEmote>(emoteId.ToString());
            var emote = Instantiate(emotePrefab, transform);
            emote.transform.localPosition = defaultEmoteLocalOffset;
            yield return emote.Animate();
            
            Animating = false;
        }
    }
}