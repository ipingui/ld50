using System.Collections;
using Extensions;
using UnityEngine;

namespace VisualNovel.Character
{
    public static class CharacterAnimationCoroutines
    {
        public static IEnumerator Bounce(Transform characterTransform, float duration)
        {
            var startPosition = characterTransform.localPosition;

            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;

                var t = timeElapsed / duration;

                var yOffset = 10*Mathf.Cos(Mathf.PI*(4*t + 1) + 1);
                characterTransform.localPosition = startPosition + yOffset * Vector3.up;
                
                yield return null;
            }
            
            yield return null;
        }

        public static IEnumerator Pulse(Transform characterTransform, float duration)
        {
            yield return characterTransform.ScaleCoroutine(1f, 1.1f, duration / 2f);
            yield return characterTransform.ScaleCoroutine(1.1f, 1f, duration / 2f);
        }
    }
}