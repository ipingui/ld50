using Enums;
using IO;
using UnityEngine;
using UnityEngine.UI;

namespace VisualNovel.Background
{
    public class BackgroundController : MonoBehaviour
    {
        [SerializeField] private Image backgroundImage;

        public void SetBackground(BackgroundId backgroundId)
        {
            var sprite = FileReader.LoadSprite(backgroundId);
            backgroundImage.sprite = sprite;
        }
    }
}