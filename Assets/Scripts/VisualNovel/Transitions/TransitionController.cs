using System.Collections;
using Game;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace VisualNovel.Transitions
{
    public class TransitionController : MonoBehaviour
    {
        [SerializeField] private Image transitionOverlay;

        private void Start()
        {
            StartCoroutine(Fade(1, 0));
        }

        public void TransitionToSceneAndIncrementDialogue(string sceneName)
        {
            ProgressionData.CurrentDialogueNumber += 1;
            StartCoroutine(TransitionToScene(sceneName));
        }

        public IEnumerator TransitionToScene(string sceneName)
        {
            yield return Fade(0, 1);
            SceneManager.LoadScene(sceneName);
        }
        
        private IEnumerator Fade(float startAlpha, float endAlpha)
        {
            const float fadeDuration = 1f;
            var timeElapsed = 0f;
            while (timeElapsed < fadeDuration)
            {
                timeElapsed += Time.deltaTime;
                
                var c = transitionOverlay.color;
                var alpha = Mathf.SmoothStep(startAlpha, endAlpha, timeElapsed / fadeDuration);
                transitionOverlay.color = new Color(c.r, c.g, c.b, alpha);
                yield return null;
            }
        }
    }
}