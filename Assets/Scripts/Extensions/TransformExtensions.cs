using System.Collections;
using UnityEngine;

namespace Extensions
{
    public static class TransformExtensions
    {
        public static IEnumerator ScaleCoroutine(this Transform transform, Vector3 startScale, Vector3 endScale, float duration)
        {
            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;

                transform.localScale = Vector3.Lerp(startScale, endScale, timeElapsed / duration);
                yield return null;
            }
        }
        
        public static IEnumerator ScaleCoroutine(this Transform transform, float startScale, float endScale, float duration)
        {
            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;

                transform.localScale = Vector3.Lerp(startScale*Vector3.one, endScale*Vector3.one, timeElapsed / duration);
                yield return null;
            }
        }
    }
}