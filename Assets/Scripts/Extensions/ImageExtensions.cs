using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Extensions
{
    public static class ImageExtensions
    {
        public static void Fade(this Image image, float startAlpha, float endAlpha, float t)
        {
            var c = image.color;
            var alpha = Mathf.SmoothStep(startAlpha, endAlpha, t);
            image.color = new Color(c.r, c.g, c.b, alpha);
        }
        
        public static IEnumerator FadeCoroutine(this Image image, float startAlpha, float endAlpha, float duration)
        {
            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;
                
                image.Fade(startAlpha, endAlpha, timeElapsed / duration);
                yield return null;
            }
        }
    }
}