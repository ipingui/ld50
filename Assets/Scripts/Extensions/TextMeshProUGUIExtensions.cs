using System.Collections;
using TMPro;
using UnityEngine;

namespace Extensions
{
    public static class TextMeshProUGUIExtensions
    {
        public static void Fade(this TextMeshProUGUI textMeshPro, float startAlpha, float endAlpha, float t)
        {
            var alpha = Mathf.SmoothStep(startAlpha, endAlpha, t);
            textMeshPro.alpha = alpha;
        }
        
        public static IEnumerator FadeCoroutine(this TextMeshProUGUI textMeshPro, float startAlpha, float endAlpha, float duration)
        {
            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;
                
                textMeshPro.Fade(startAlpha, endAlpha, timeElapsed / duration);
                yield return null;
            }
        }
    }
}