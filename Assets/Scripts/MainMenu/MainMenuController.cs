using System;
using Enums;
using Game;
using Sound;
using TMPro;
using UnityEngine;
using VisualNovel.Transitions;

namespace MainMenu
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private TransitionController transitionController;
        [SerializeField] private TMP_InputField playerNameField;
        [SerializeField] private TMP_InputField girlfriendNameField;
        [SerializeField] private EnterNamesWarning enterNamesWarning;
        [SerializeField] private TMP_Dropdown colorBlindAssistDropdown;

        private bool gameStarted;

        private void Start()
        {
            MusicPlayer.Instance.Play(MusicId.Happy);
        }

        public void StartGame()
        {
            if (gameStarted || !NamesAreValid())
            {
                SfxManager.Instance.PlaySound(SfxId.Negative);
                enterNamesWarning.Pulse();
                return;
            }

            ProgressionData.CurrentDialogueNumber = 0;
            NameData.SetNames(girlfriendNameField.text, playerNameField.text);
            ProgressionData.ColorBlindType = colorBlindAssistDropdown.captionText.text;
            SfxManager.Instance.PlaySound(SfxId.Positive);
            gameStarted = true;
            StartCoroutine(transitionController.TransitionToScene("HowToPlay"));
        }

        private bool NamesAreValid()
        {
            return playerNameField.text != string.Empty && girlfriendNameField.text != string.Empty;
        }
    }
}