using System.Collections;
using Extensions;
using TMPro;
using UnityEngine;

namespace MainMenu
{
    public class EnterNamesWarning : MonoBehaviour
    {
        private Coroutine pulseCoroutine;
        private bool triggered;
        
        public void Pulse()
        {
            if (pulseCoroutine != null)
            {
                StopCoroutine(pulseCoroutine);
            }

            if (!triggered)
            {
                var textMeshPro = GetComponent<TextMeshProUGUI>();
                StartCoroutine(textMeshPro.FadeCoroutine(0f, 1f, 0.2f));
            }

            pulseCoroutine = StartCoroutine(PulseCoroutine());
            triggered = true;
        }

        private IEnumerator PulseCoroutine()
        {
            const float duration = 0.4f;
            yield return transform.ScaleCoroutine(1f, 1.2f, duration/2f);
            yield return transform.ScaleCoroutine(1.2f, 1f, duration/2f);
        }
    }
}