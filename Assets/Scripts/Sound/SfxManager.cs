using Enums;
using IO;
using PinguTools;

namespace Sound
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class SfxManager : MonoBehaviour
    {
        private AudioSource player;

        private readonly Dictionary<SfxId, AudioClip> sounds = new Dictionary<SfxId, AudioClip>();

        public static SfxManager Instance { get; private set; } = null;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        
            player = GetComponent<AudioSource>();
            LoadSounds();
            
            DontDestroyOnLoad(gameObject);
        }

        private void LoadSounds()
        {
            var sfxEnums = Enum.GetValues(typeof(SfxId)).Cast<SfxId>();
        
            foreach (var soundEffectId in sfxEnums)
            {
                var soundEffect = FileReader.LoadSfx(soundEffectId);
                sounds.Add(soundEffectId, soundEffect);
            }
        }

        public void PlaySound(SfxId sfxId)
        {
            player.pitch = 1;
            var clip = sounds[sfxId];
            player.PlayOneShot(clip);
        }

        public void PlaySoundWithRandomPitch(SfxId sfxId)
        {
            var pitch = MathTools.RandomFloat() + 0.5f;
            player.pitch = pitch;
            var clip = sounds[sfxId];
            player.PlayOneShot(clip);    
        }
    }

}