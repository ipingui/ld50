using Enums;
using IO;

namespace Sound
{
    using System.Collections.Generic;
    using UnityEngine;

    public class MusicPlayer : MonoBehaviour
    {
        private AudioSource _player;

        private AudioSource _audioSource;
        private MusicId _currentMusic;

        private Dictionary<MusicId, AudioClip> cachedMusic;
    
        public static MusicPlayer Instance { get; private set; }

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            cachedMusic = new Dictionary<MusicId, AudioClip>();
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public void Play(MusicId musicID)
        {
            if (_currentMusic == musicID)
            {
                return;
            }

            if (musicID == MusicId.Pause)
            {
                Pause();
                return;
            }

            if (musicID == MusicId.Resume)
            {
                Resume();
                return;
            }
        
            _currentMusic = musicID;
        
            AudioClip clip;
            if (cachedMusic.ContainsKey(musicID))
            {
                clip = cachedMusic[musicID];
            }
            else
            {
                clip = FileReader.LoadMusic(musicID);
                cachedMusic.Add(musicID, clip);
            }
        
            _audioSource.clip = clip;
            _audioSource.Play();
        }

        public void ClearCache()
        {
            cachedMusic.Clear();
        }

        public void PreLoadMusic(MusicId musicID)
        {
            if (cachedMusic.ContainsKey(musicID))
            {
                return;
            }

            var clip = FileReader.LoadMusic(musicID);
            cachedMusic.Add(musicID, clip);
            Debug.Log($"Added Music {musicID} to MusicPlayer cache.");
        }

        public void Pause()
        {
            _audioSource.Pause();
        }

        public void Resume()
        {
            _audioSource.Play();
        }

        public void SetVolume(float volume)
        {
            _audioSource.volume = volume;
        }
    }

}