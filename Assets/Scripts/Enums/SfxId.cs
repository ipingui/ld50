namespace Enums
{
    public enum SfxId
    {
        None,
        
        Shock,
        Weird,
        
        DialogueAdvance,
        ChooseAnswer,
        TextBloop,
        Negative,
        Positive,
        Hover,
        Hit,
        Select,
        Shoot,
        HealthBarSpawn,
        Hurt,
        GirlTextBloop
    }
}