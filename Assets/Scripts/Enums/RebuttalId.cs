namespace Enums
{
    public enum RebuttalId
    {
        Expensive,
        TimeConsuming,
        Responsibility,
        Allergies
    }
}