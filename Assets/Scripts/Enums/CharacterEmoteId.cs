namespace Enums
{
    public enum CharacterEmoteId
    {
        None,
        
        Angry,
        Sad,
        Happy,
        Sparkle,
        Sweat,
        Love
    }
}