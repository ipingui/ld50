namespace Enums
{
    public enum MusicId
    {
        None,
        Pause,
        Resume,
        Stop,
        
        Happy,
        Scary,
        Sad,
        Minigame
    }
}