namespace Enums
{
    public enum CharacterAnimationId
    {
        None,
        
        Bounce,
        Shake,
        Pulse
    }
}