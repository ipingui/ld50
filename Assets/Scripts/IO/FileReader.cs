using Enums;
using UnityEngine;

namespace IO
{
    public static class FileReader
    {
        public static T LoadScriptableObject<T>(string path)
        where T : ScriptableObject
        {
            return Resources.Load<T>(path);
        }
        
        public static T LoadPrefab<T>(string prefabName)
            where T : MonoBehaviour
        {
            return Resources.Load<T>($"prefabs/{prefabName}");
        }
        
        public static Sprite LoadSprite(BackgroundId backgroundId)
        {
            var path = $"backgrounds/{backgroundId}";
            return LoadSprite(path);
        }
        
        public static Sprite LoadSprite(CharacterSpriteId characterSpriteId)
        {
            var path = $"character/{characterSpriteId}";
            return LoadSprite(path);
        }
        
        public static AudioClip LoadSfx(SfxId sfxId)
        {
            return Resources.Load<AudioClip>($"sfx/{sfxId}");
        }
        
        public static AudioClip LoadMusic(MusicId musicId)
        {
            return Resources.Load<AudioClip>($"music/{musicId}");
        }
        
        private static Sprite LoadSprite(string filePath)
        {
            return Resources.Load<Sprite>($"sprites/{filePath}");
        }
    }
}