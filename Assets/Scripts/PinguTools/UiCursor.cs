using UnityEngine;

namespace PinguTools
{
    using UnityEngine;

    public abstract class UiCursor : MonoBehaviour
    {
        private Vector3 MousePosition => Input.mousePosition;

        private const float MovementSpeed = 0.5f;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && IsClickConditionSatisfied())
            {
                OnClick();
            }
        
            FollowMouse();
        }

        protected abstract void OnClick();
        protected abstract bool IsClickConditionSatisfied();

        private void FollowMouse()
        {
            transform.position = Vector3.Lerp(transform.position, MousePosition, MovementSpeed);
        }
    }

}