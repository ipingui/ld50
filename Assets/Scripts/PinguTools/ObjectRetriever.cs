namespace PinguTools
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class ObjectRetriever
    {
        public static T GetFirstObjectOfTypeAtLocation<T>(Vector2 targetPosition) where T : MonoBehaviour
        {
            var pointerEventData = new PointerEventData(EventSystem.current)
            {
                position = targetPosition
            };

            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, results);

            return results
                .Select(r => r.gameObject.GetComponent<T>())
                .FirstOrDefault(w => w != null);
        }
    }
}