namespace Game
{
    public static class ProgressionData
    {
        public static int CurrentDialogueNumber { get; set; }
        public static int LevelReached { get; set; }
        public static string ColorBlindType { get; set; }
    }
}