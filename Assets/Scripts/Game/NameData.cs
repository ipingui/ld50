using System.Collections.Generic;
using Enums;

namespace Game
{
    public static class NameData
    {
        private static Dictionary<CharacterId, string> characterNames = new Dictionary<CharacterId, string>()
        {
            {CharacterId.Dog, "Dog"}
        };

        public static void SetNames(string girlName, string playerName)
        {
            characterNames[CharacterId.Girl] = girlName;
            characterNames[CharacterId.Player] = playerName;
        }

        public static string GetName(CharacterId characterId)
        {
            if (characterNames.ContainsKey(characterId))
            {
                return characterNames[characterId];
            }

            return string.Empty;
        }
    }
}