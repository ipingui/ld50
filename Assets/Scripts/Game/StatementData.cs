using System.Collections.Generic;
using System.Linq;
using Enums;

namespace Game
{
    public static class StatementData
    {
        private static Dictionary<string, CharacterSpriteId> statements = new Dictionary<string, CharacterSpriteId>()
        {
            {"But he's so cute!", CharacterSpriteId.Happy},
            {"I just wanna boop his nose!", CharacterSpriteId.Happy},
            {"Don't you like dogs?", CharacterSpriteId.Angry},
            {"I'm only a little allergic...", CharacterSpriteId.Shock},
            {"Think of all the fun!", CharacterSpriteId.Happy},
            {"I promise I'd look after him!", CharacterSpriteId.Shock},
            {"Dog walks are good exercise!", CharacterSpriteId.Happy},
            {"Cmoooooon...", CharacterSpriteId.Shock},
        };

        public static List<string> GetAll()
        {
            return statements.Keys.ToList();
        }

        public static CharacterSpriteId GetCharacterSprite(string statement)
        {
            return statements[statement];
        }
    }
}