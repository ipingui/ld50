using System.Collections.Generic;
using Enums;
using Minigame.Models;
using UnityEngine;

namespace Game
{
    public static class RebuttalData
    {
        private static Dictionary<RebuttalId, Rebuttal> rebuttals = new Dictionary<RebuttalId, Rebuttal>()
        {
            {RebuttalId.Expensive, new Rebuttal(RebuttalId.Expensive, "Expensive")},
            {RebuttalId.Responsibility, new Rebuttal(RebuttalId.Responsibility, "Responsibility")},
            {RebuttalId.TimeConsuming, new Rebuttal(RebuttalId.TimeConsuming, "Time Consuming")},
            {RebuttalId.Allergies, new Rebuttal(RebuttalId.Allergies, "Allergies")}
        };

        private static Dictionary<RebuttalId, Color> rebuttalColorsProtanopia = new Dictionary<RebuttalId, Color>()
        {
            {RebuttalId.Expensive, RgbToColor(174, 156, 69)},
            {RebuttalId.Responsibility, RgbToColor(96, 115, 177)},
            {RebuttalId.TimeConsuming, RgbToColor(167, 184, 248)},
            {RebuttalId.Allergies, RgbToColor(5, 41, 85)}
        };
        
        private static Dictionary<RebuttalId, Color> rebuttalColorsDeuteranopia = new Dictionary<RebuttalId, Color>()
        {
            {RebuttalId.Expensive, RgbToColor(197, 148, 52)},
            {RebuttalId.Responsibility, RgbToColor(96, 115, 177)},
            {RebuttalId.TimeConsuming, RgbToColor(163, 183, 249)},
            {RebuttalId.Allergies, RgbToColor(5, 41, 85)}
        };
        
        private static Dictionary<RebuttalId, Color> rebuttalColors = new Dictionary<RebuttalId, Color>()
        {
            {RebuttalId.Expensive, Color.green},
            {RebuttalId.Responsibility, Color.red},
            {RebuttalId.TimeConsuming, Color.yellow},
            {RebuttalId.Allergies, Color.cyan}
        };

        public static Rebuttal GetRebuttal(RebuttalId rebuttalId)
        {
            return rebuttals[rebuttalId];
        }

        public static Color GetRebuttalColor(RebuttalId rebuttalId)
        {
            switch (ProgressionData.ColorBlindType)
            {
                case "None":
                    return rebuttalColors[rebuttalId];
                case "Protanopia":
                    return rebuttalColorsProtanopia[rebuttalId];
                case "Deuteranopia":
                    return rebuttalColorsDeuteranopia[rebuttalId];
            }
            
            return rebuttalColors[rebuttalId];
        }

        private static Color RgbToColor(float r, float g, float b)
        {
            return new Color(r / 255f, g / 255f, b / 255f);
        }
    }
}