using Enums;
using Sound;
using UnityEngine;
using VisualNovel.Transitions;

namespace HowToPlay
{
    public class HowToPlayScreen : MonoBehaviour
    {
        private bool started;
        
        public void StartGame()
        {
            if (started)
            {
                return;
            }
            
            var transitionController = FindObjectOfType<TransitionController>();
            StartCoroutine(transitionController.TransitionToScene("VisualNovelScene"));
            started = true;
            SfxManager.Instance.PlaySound(SfxId.Positive);
        }
    }
}