using System;
using System.Collections;
using Enums;
using Extensions;
using Game;
using Sound;
using TMPro;
using UnityEngine;
using VisualNovel.Transitions;

namespace Ending
{
    public class LoseEndingScreen : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI levelNumberText;

        private void Awake()
        {
            if (levelNumberText == null)
            {
                return;
            }
            
            levelNumberText.text = ProgressionData.LevelReached.ToString();
            levelNumberText.alpha = 0f;
            StartCoroutine(LevelNumberAnimation());
        }

        public void PlayAgain()
        {
            var transitionController = FindObjectOfType<TransitionController>();
            StartCoroutine(transitionController.TransitionToScene("MainMenuScene"));
        }

        private IEnumerator LevelNumberAnimation()
        {
            yield return new WaitForSeconds(1.5f);
            SfxManager.Instance.PlaySound(SfxId.HealthBarSpawn);
            yield return levelNumberText.FadeCoroutine(0, 1, 0.2f);
        }
    }
}