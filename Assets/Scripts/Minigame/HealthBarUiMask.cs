using System;
using System.Collections;
using Enums;
using Extensions;
using Sound;
using TMPro;
using UnityEngine;

namespace Minigame
{
    public class HealthBarUiMask : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI hpText;
        
        private void Start()
        {
            StartCoroutine(Appear());
        }

        private IEnumerator Appear()
        {
            var startScale = new Vector3(1, 0, 0);
            transform.localScale = startScale;
            hpText.alpha = 0f;
            yield return new WaitForSeconds(1.5f);
            SfxManager.Instance.PlaySound(SfxId.HealthBarSpawn);
            StartCoroutine(hpText.FadeCoroutine(0f, 1f, 0.2f));
            yield return transform.ScaleCoroutine(startScale, Vector3.one, 0.2f);
        }
    }
}