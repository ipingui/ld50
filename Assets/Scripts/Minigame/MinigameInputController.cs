using System.Collections;
using Enums;
using Extensions;
using IO;
using Sound;
using UnityEngine;

namespace Minigame
{
    public class MinigameInputController : MonoBehaviour
    {
        [SerializeField] private Barrel barrel;
        
        private bool canShoot;

        private void Awake()
        {
            canShoot = true;
        }

        private void Update()
        {
            HandleRebuttalShootingInput();
        }

        private void HandleRebuttalShootingInput()
        {
            if (!canShoot)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Shoot();
            }
        }

        private void Shoot()
        {
            SfxManager.Instance.PlaySoundWithRandomPitch(SfxId.Shoot);
            StartCoroutine(ShootAnimation());
            StartCoroutine(ShootCooldown());
        }

        private IEnumerator ShootAnimation()
        {
            const float shootAnimationDuration = 1f;
            var bulletPrefab = FileReader.LoadPrefab<BulletObject>("Bullet");
            var bullet = Instantiate(bulletPrefab, transform);
            
            var clickPosition = transform.InverseTransformPoint(Input.mousePosition);
            var startPosition = clickPosition + new Vector3(200f, -200f, 0);
            bullet.transform.localPosition = startPosition;
            var chosenRebuttal = barrel.GetCurrentRebuttal().RebuttalId;

            var targetScale = 0.15f;

            var timeElapsed = 0f;
            while (timeElapsed < shootAnimationDuration)
            {
                timeElapsed += Time.deltaTime;

                var t = timeElapsed / shootAnimationDuration;
                bullet.transform.localPosition = Vector3.Lerp(startPosition, clickPosition, t);
                var scale = Mathf.SmoothStep(1, targetScale, t);
                bullet.transform.localScale = scale*Vector3.one;
                yield return null;
            }

            bullet.Check(chosenRebuttal);
            yield return bullet.BulletImage.FadeCoroutine(1, 0, 0.2f);
            Destroy(bullet.gameObject);
        }

        private IEnumerator ShootCooldown()
        {
            const float shootCooldown = 0.5f;
            canShoot = false;
            yield return new WaitForSeconds(shootCooldown);
            canShoot = true;
        }
    }
}