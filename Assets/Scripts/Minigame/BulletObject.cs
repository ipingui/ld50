using System;
using Enums;
using Extensions;
using Minigame.Girl;
using PinguTools;
using UnityEngine;
using UnityEngine.UI;

namespace Minigame
{
    public class BulletObject : MonoBehaviour
    {
        [SerializeField] private Image bulletImage;

        public Image BulletImage => bulletImage;

        private void Awake()
        {
            StartCoroutine(bulletImage.FadeCoroutine(0, 1, 0.2f));
        }

        public void Check(RebuttalId rebuttalId)
        {
            var statement = ObjectRetriever.GetFirstObjectOfTypeAtLocation<StatementObject>(transform.position);
            if (!statement)
            {
                return;
            }
            
            statement.OnShot(rebuttalId);
        }
    }
}