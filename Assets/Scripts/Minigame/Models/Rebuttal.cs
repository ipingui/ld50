using Enums;

namespace Minigame.Models
{
    public class Rebuttal
    {
        public RebuttalId RebuttalId { get; }
        public string Title { get; }

        public Rebuttal(RebuttalId rebuttalId, string title)
        {
            RebuttalId = rebuttalId;
            Title = title;
        }
    }
}