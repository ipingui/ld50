using Enums;

namespace Minigame.Models
{
    public class Statement
    {
        public string Title { get; set; }
        public RebuttalId Weakness { get; set; }

        public Statement(string title, RebuttalId weakness)
        {
            Title = title;
            Weakness = weakness;
        }
    }
}