using Game;
using Minigame.Models;
using TMPro;
using UnityEngine;

namespace Minigame
{
    public class RebuttalUi : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;

        public void SetRebuttal(Rebuttal rebuttal)
        {
            title.text = rebuttal.Title;
            title.color = RebuttalData.GetRebuttalColor(rebuttal.RebuttalId);
        }
    }
}