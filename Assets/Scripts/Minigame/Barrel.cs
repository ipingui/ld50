using System;
using System.Collections;
using System.Collections.Generic;
using Enums;
using Game;
using IO;
using Minigame.Models;
using PinguTools;
using Sound;
using UnityEngine;

namespace Minigame
{
    public class Barrel : MonoBehaviour
    {
        [SerializeField] private RebuttalUi currentRebuttal;
        [SerializeField] private Vector3 rebuttalOffScreenPos;
        [SerializeField] private Vector3 rebuttalOnScreenPos;
        
        private bool inputAllowed;

        private int currentIndex;
        private List<Rebuttal> rebuttals = new List<Rebuttal>();
        private Vector3 rotationAxis;
        
        private void Awake()
        {
            Initialise();
            inputAllowed = true;
        }

        private void Start()
        {
            SetCurrent(currentIndex);
        }

        private void Update()
        {
            CheckForInput();
        }

        public Rebuttal GetCurrentRebuttal()
        {
            return rebuttals[currentIndex];
        }
        
        private void CheckForInput()
        {
            if (!inputAllowed)
            {
                return;
            }
        
            var scrollWheelValue = Input.GetAxis("Mouse ScrollWheel");
            if (scrollWheelValue > 0)
            {
                ChangeBarrelIndex(1);
                return;
            }
            else if (scrollWheelValue < 0)
            {
                ChangeBarrelIndex(-1);
                return;
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                ChangeBarrelIndex(1);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                ChangeBarrelIndex(-1);
            }
        }
        
        private void ChangeBarrelIndex(int change)
        {
            var indexBefore = currentIndex;
        
            if (change < 0)
            {
                currentIndex = Math.Max(0, currentIndex + change);
            }
            else if (change > 0)
            {
                currentIndex = Math.Min(rebuttals.Count - 1, currentIndex + change);
            }

            if (currentIndex == indexBefore)
            {
                return;
            }

            StartCoroutine(ShowRebuttal(currentIndex));
        }

        private void Initialise()
        {
            var allRebuttals = MathTools.EnumToList<RebuttalId>();
            foreach (var rebuttal in allRebuttals)
            {
                var rebuttalData = RebuttalData.GetRebuttal(rebuttal);
                rebuttals.Add(rebuttalData);
            }
        }

        private IEnumerator ShowRebuttal(int index)
        {
            const float animationDuration = 0.25f;
            
            SfxManager.Instance.PlaySound(SfxId.Select);
            
            var halfDuration = animationDuration / 2f;

            var timeElapsed = 0f;
            while (timeElapsed < halfDuration)
            {
                timeElapsed += Time.deltaTime;

                currentRebuttal.transform.localPosition =
                    Vector3.Lerp(rebuttalOnScreenPos, rebuttalOffScreenPos, timeElapsed / halfDuration);
                yield return null;
            }
            
            SetCurrent(index);

            while (timeElapsed < animationDuration)
            {
                timeElapsed += Time.deltaTime;

                currentRebuttal.transform.localPosition =
                    Vector3.Lerp(rebuttalOffScreenPos, rebuttalOnScreenPos, timeElapsed / animationDuration);
                yield return null;
            }
        }

        private void SetCurrent(int index)
        {
            var rebuttal = rebuttals[index];
            currentRebuttal.SetRebuttal(rebuttal);
        }
    }
}