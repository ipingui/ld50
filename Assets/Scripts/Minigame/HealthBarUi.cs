using System.Collections;
using Minigame.Girl;
using UnityEngine;
using UnityEngine.UI;

namespace Minigame
{
    public class HealthBarUi : MonoBehaviour
    {
        [SerializeField] private Image healthBar;

        public void MoveToValue(int value)
        {
            var xScale = value / (float) MinigameController.MaxHealth;
            StartCoroutine(MoveBar(xScale));
        }

        private IEnumerator MoveBar(float targetXScale)
        {
            const float duration = 0.25f;
            var startXScale = healthBar.transform.localScale.x;

            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;
                
                var s = healthBar.transform.localScale;
                var newX = Mathf.SmoothStep(startXScale, targetXScale, timeElapsed / duration);
                healthBar.transform.localScale = new Vector3(newX, s.y, s.z);
                yield return null;
            }
        }
    }
}