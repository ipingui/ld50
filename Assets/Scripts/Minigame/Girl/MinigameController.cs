using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enums;
using Game;
using IO;
using Minigame.Models;
using PinguTools;
using Sound;
using UnityEngine;
using VisualNovel.Character;
using VisualNovel.Transitions;
using CharacterController = VisualNovel.Character.CharacterController;

namespace Minigame.Girl
{
    public class MinigameController : MonoBehaviour
    {
        public const int MaxHealth = 10;
        private const float StatementTargetWheelRadius = 280f;
        
        [SerializeField] private HealthBarUi healthBarUi;
        [SerializeField] private CharacterAnimationController characterAnimationController;
        [SerializeField] private CharacterController characterController;
        
        public List<StatementTarget> StatementTargets { get; private set; } = new List<StatementTarget>();

        private float interval = 3f;
        private bool gameRunning;

        private int targetShots = 10;
        private int numberOfShots;
        private int currentHealth;

        private void Start()
        {
            currentHealth = MaxHealth;
            InitialiseStatementTargets();
            characterController.SetCharacterSprite(CharacterSpriteId.Normal);
            StartCoroutine(StatementLoop());
        }

        public void RegisterShot()
        {
            numberOfShots += 1;
            if (numberOfShots >= targetShots)
            {
                gameRunning = false;
            }
        }

        public void RegisterMiss()
        {
            currentHealth -= 1;
            if (currentHealth < 0)
            {
                currentHealth = 0;
            }
            
            SfxManager.Instance.PlaySoundWithRandomPitch(SfxId.Hurt);
            healthBarUi.MoveToValue(currentHealth);

            if (currentHealth == 0)
            {
                gameRunning = false;
            }
        }

        private void InitialiseStatementTargets()
        {
            const int numberOfTargets = 60;
            for (var i = 0; i < numberOfTargets; i++)
            {
                InstantiateTarget(i, numberOfTargets);
            }
        }

        private void InstantiateTarget(int i, int numberOfTargets)
        {
            var targetPrefab = FileReader.LoadPrefab<StatementTarget>("StatementTarget");
            var target = Instantiate(targetPrefab, transform);
            target.transform.localPosition = Vector3.zero;

            var angle = 360 / numberOfTargets;
            target.transform.Rotate(new Vector3(0, 0, angle*i));
            target.transform.localPosition = target.transform.up * StatementTargetWheelRadius;
            StatementTargets.Add(target);
        }

        private IEnumerator StatementLoop()
        {
            gameRunning = true;
            
            while (gameRunning)
            {
                yield return new WaitForSeconds(interval);
                ShootRandomStatement();
                DoRandomAnimation();
            }

            var transitionController = FindObjectOfType<TransitionController>();
            var statementObjects = FindObjectsOfType<StatementObject>();
            foreach (var statementObject in statementObjects)
            {
                statementObject.Remove();
            }
            
            ProgressionData.LevelReached = ProgressionData.CurrentDialogueNumber + 1;

            if (numberOfShots >= targetShots)
            {
                yield return WinAnimation();
                transitionController.TransitionToSceneAndIncrementDialogue("VisualNovelScene");
            }
            else
            {
                yield return LoseAnimation();
                ProgressionData.CurrentDialogueNumber = -1;
                yield return transitionController.TransitionToScene("VisualNovelScene");
            }
        }

        private IEnumerator WinAnimation()
        {
            Debug.Log("Win");
            yield return new WaitForSeconds(1f);
        }

        private IEnumerator LoseAnimation()
        {
            Debug.Log("Lose");
            characterController.SetCharacterSprite(CharacterSpriteId.Happy);
            yield return characterAnimationController.DoAnimation(CharacterAnimationId.Bounce);
            yield return characterAnimationController.DoAnimation(CharacterAnimationId.Bounce);
        }

        private void ShootRandomStatement()
        {
            var statements = StatementData.GetAll();
            var randomStatement = MathTools.RandomChoice(statements);
            var statementPrefab = FileReader.LoadPrefab<StatementObject>("Statement");
            characterController.SetCharacterSprite(StatementData.GetCharacterSprite(randomStatement));
            var statement = Instantiate(statementPrefab, transform);

            var allRebuttals = MathTools.EnumToList<RebuttalId>();
            var statementWitRandomWeakness = new Statement(randomStatement, MathTools.RandomChoice(allRebuttals));
            statement.Initialise(statementWitRandomWeakness, this);
        }

        private void DoRandomAnimation()
        {
            var animations = MathTools.EnumToList<CharacterAnimationId>().Where(a => a != CharacterAnimationId.None).ToList();
            var randomAnimation = MathTools.RandomChoice(animations);
            StartCoroutine(characterAnimationController.DoAnimation(randomAnimation));
        }
    }
}