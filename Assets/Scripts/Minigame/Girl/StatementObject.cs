using System.Collections;
using Enums;
using Extensions;
using Game;
using Minigame.Models;
using PinguTools;
using Sound;
using TMPro;
using UnityEngine;

namespace Minigame.Girl
{
    public class StatementObject : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI statementText;
        private MinigameController _minigameController;

        private Statement _statement;
        private Coroutine moveCoroutine;
        private bool destroyed;
        
        public void Initialise(Statement statement, MinigameController minigameController)
        {
            _minigameController = minigameController;

            statementText.text = statement.Title;
            _statement = statement;
            statementText.color = RebuttalData.GetRebuttalColor(statement.Weakness);

            StartCoroutine(Fade(0, 1));
            moveCoroutine = StartCoroutine(Move());
        }

        public void OnShot(RebuttalId rebuttalId)
        {
            if (rebuttalId != _statement.Weakness)
            {
                return;
            }
            
            _minigameController.RegisterShot();
            SfxManager.Instance.PlaySoundWithRandomPitch(SfxId.Hit);
            StartCoroutine(ShatterText(0.5f));
        }

        public void Remove()
        {
            StartCoroutine(ShatterText(0.1f));
        }

        private void OnMiss()
        {
            if (destroyed)
            {
                return;
            }
            
            _minigameController.RegisterMiss();
        }

        private IEnumerator Move()
        {
            var denominator = Mathf.Max(ProgressionData.CurrentDialogueNumber + 1, 1);
            var duration = 10 / denominator;
            transform.position = Vector3.zero;

            var target = MathTools.RandomChoice(_minigameController.StatementTargets);

            var startRotation = transform.localRotation;
            var rotationDirection = MathTools.RandomBool() ? 1 : -1;
            var rotationAmount = new Vector3(0, 0, MathTools.RandomInt(30, 70));
            var targetRotation = Quaternion.Euler(transform.eulerAngles + rotationDirection * rotationAmount);
            
            var timeElapsed = 0f;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;

                var t = timeElapsed / duration;

                transform.localPosition = Vector3.Lerp(Vector3.zero, target.transform.localPosition, t);
                transform.localRotation = Quaternion.Lerp(startRotation, targetRotation, t);
                yield return null;
            }
            
            OnMiss();

            yield return statementText.FadeCoroutine(1, 0, 0.2f);
        }
        
        private IEnumerator Fade(float startAlpha, float endAlpha)
        {
            const float fadeDuration = 0.3f;
            var timeElapsed = 0f;
            while (timeElapsed < fadeDuration)
            {
                timeElapsed += Time.deltaTime;
                
                var alpha = Mathf.SmoothStep(startAlpha, endAlpha, timeElapsed / fadeDuration);
                statementText.alpha = alpha;
                yield return null;
            }
        }

        private IEnumerator ShatterText(float duration)
        {
            destroyed = true;
            StopCoroutine(moveCoroutine);
            StartCoroutine(statementText.FadeCoroutine(1, 0, duration));
            yield return transform.ScaleCoroutine(Vector3.one, 1.3f * Vector3.one, duration);
            Destroy(gameObject);
        }
    }
}